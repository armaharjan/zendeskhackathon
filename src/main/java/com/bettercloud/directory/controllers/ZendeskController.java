package com.bettercloud.directory.controllers;

import com.bettercloud.directory.Main;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Level;
import java.util.logging.Logger;

@Api(value = "User", description = "Slack USER API")
@RestController
@RequestMapping(value = "/zendesk")
public class ZendeskController {
    private static final Logger LOGGER = Logger.getLogger(UserController.class.getSimpleName());
    private static final String UPDATE_URL = Main.getEnvironmentOverlayProperties().get("bettercloud.zendesk.update.ticket");
    private static final String API_TOKEN = Main.getEnvironmentOverlayProperties().get("bettercloud.zendesk.apitoken");
    private static final String USER_NAME = Main.getEnvironmentOverlayProperties().get("bettercloud.zendesk.userName");

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    SlackController slackController;

    @ApiOperation(value = "Solve ticket in Zendesk", notes = "Solves ticket in zendesk.")
    @RequestMapping(value = "/solved/", method = RequestMethod.POST)
    public String solved(@RequestParam(value = "parameters", required = true) String parameters) {
        String splitParams[] = parameters.split("\\|");
        String updateEndPoint = String.format(UPDATE_URL+"/%s", splitParams[1]);
        slackController.writeMessageToChannel(splitParams[1], splitParams[0].split(",")[1]);
        return deleteTicketFromZendesk(updateEndPoint, buildTicketNode(splitParams[0]));
    }

    @ApiOperation(value = "Updates ticket in Zendesk", notes = "Updates ticket in zendesk.")
    @RequestMapping(value = "/update/", method = RequestMethod.POST)
    public String update(@RequestParam(value = "parameters", required = true) String parameters) {
        String splitParams[] = parameters.split("\\|");
        String updateEndPoint = String.format(UPDATE_URL+"/%s", splitParams[1]);
        slackController.writeMessageToChannel(splitParams[1], splitParams[0].split(",")[1]);
        return updateZendesk(updateEndPoint, buildTicketNode(splitParams[0]));
    }

    private String updateZendesk(String proxyPath, JsonNode updateNode){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        headers.set("Authorization", "Basic YXJjaGFuYS5tYWhhcmphbkBiZXR0ZXJjbG91ZC5jb20vdG9rZW46VWVkaWJwaG5OM1FtemZVWXltSU1lQU02N3BON2NhTnR2SDZ0UjJPOA==");
        org.springframework.http.HttpEntity httpEntity = new org.springframework.http.HttpEntity(updateNode, headers);
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        try {
            ResponseEntity<?> responseEntity = restTemplate.exchange(proxyPath, HttpMethod.PUT, httpEntity, String.class);
            return responseEntity.getBody().toString();
        } catch (HttpServerErrorException e) {
            LOGGER.log(Level.SEVERE, "Couldn't update ticket in zendesk.", e);
            return "Couldn't update ticket in zendesk.";
        }
    }

    private String deleteTicketFromZendesk(String proxyPath, JsonNode updateNode){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        headers.set("Authorization", "Basic YXJjaGFuYS5tYWhhcmphbkBiZXR0ZXJjbG91ZC5jb20vdG9rZW46VWVkaWJwaG5OM1FtemZVWXltSU1lQU02N3BON2NhTnR2SDZ0UjJPOA==");
        org.springframework.http.HttpEntity httpEntity = new org.springframework.http.HttpEntity(updateNode, headers);
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        try {
            ResponseEntity<?> responseEntity = restTemplate.exchange(proxyPath, HttpMethod.PUT, httpEntity, String.class);
            return responseEntity.getBody().toString();
        } catch (HttpServerErrorException e) {
            LOGGER.log(Level.SEVERE, "Couldn't update ticket in zendesk.", e);
            return "Error updating ticket in zendesk.";
        }
    }

    private JsonNode buildDeleteNode(String parameters){
        ObjectMapper mapper = new ObjectMapper();
        JsonNodeFactory jsonNodeFactory = mapper.getNodeFactory();
        ObjectNode deleteNode = jsonNodeFactory.objectNode();
        deleteNode.put("status", parameters);
        return deleteNode;
    }

    private JsonNode buildTicketNode(String parameters){
        String ticketParams = parameters.replaceAll("\\s\\s+", " ").replaceAll("^\\s", "");
        String ticketParameters[] = ticketParams.split(",");
        ObjectMapper mapper = new ObjectMapper();
        JsonNodeFactory jsonNodeFactory = mapper.getNodeFactory();
        ObjectNode ticketNode = jsonNodeFactory.objectNode();
        ticketNode.put("status", ticketParameters[0]);

        ObjectNode commentNode = jsonNodeFactory.objectNode();
        commentNode.put("body", ticketParameters[1]);

        commentNode.put("author_id", Long.parseLong(ticketParameters[2]));
        ticketNode.set("comment", commentNode);

        ObjectNode node = jsonNodeFactory.objectNode();
        return node.set("ticket", ticketNode);
    }
}
