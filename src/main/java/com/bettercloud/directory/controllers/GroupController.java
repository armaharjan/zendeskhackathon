package com.bettercloud.directory.controllers;

import com.bettercloud.directory.Main;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

@Api(value = "Group", description = "Slack Group API")
@RestController
@RequestMapping(value = "/group")
public class GroupController {
    private static final Logger LOGGER = Logger.getLogger(GroupController.class.getSimpleName());
    protected static final String SLACK_API_TOKEN = Main.getEnvironmentOverlayProperties().get("bettercloud.slack.apitoken");

    @Autowired
    RestTemplate restTemplate;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ApiOperation(value = "Create Group", notes = "Creates a new group.")
    public String createGroup(@RequestParam(value = "groupName", required = true) String groupName) {
        String slackEndPoint = String.format(Main.getEnvironmentOverlayProperties().get("bettercloud.slack.create.group") + "?token=%s&name=%s", SLACK_API_TOKEN, groupName.replace("\"", ""));
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        org.springframework.http.HttpEntity httpEntity = new org.springframework.http.HttpEntity(headers);
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        try {
            ResponseEntity<?> responseEntity = restTemplate.exchange(slackEndPoint, HttpMethod.POST, httpEntity, String.class);
            return responseEntity.getBody().toString();
        } catch (HttpServerErrorException e) {
            LOGGER.log(Level.SEVERE, "Couldn't create group in slack", e);
            return "Couldn't create group in slack";
        }
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ApiOperation(value = "Delete Group", notes = "Deletes a group.")
    public String deleteGroup(@RequestParam(value = "groupName", required = true) String groupName) {
        String groupId = getGroupId(groupName).replaceAll("\"", "");
        if(groupId.contains("GroupNotFound") || groupId.contains("Error")){
            return "Group already archived.";
        }
        String archiveEndPoint = String.format(Main.getEnvironmentOverlayProperties().get("bettercloud.slack.archive.group") + "?token=%s&channel=%s", SLACK_API_TOKEN, groupId);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        org.springframework.http.HttpEntity httpEntity = new org.springframework.http.HttpEntity(headers);
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        try {
            ResponseEntity<?> responseEntity = restTemplate.exchange(archiveEndPoint, HttpMethod.POST, httpEntity, String.class);
//           return responseEntity.getBody().toString();
            return closeChannel(groupId);
        } catch (HttpServerErrorException e) {
            LOGGER.log(Level.SEVERE, "Couldn't create group in slack", e);
            return "Couldn't create group in slack";
        }

    }

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "Get Group Id", notes = "Gets a group Id.")
    public String getGroupId(@RequestParam(value = "groupName", required = true) String groupName) {
        String getChannelsEndPoint = String.format(Main.getEnvironmentOverlayProperties().get("bettercloud.slack.groupList") + "?token=%s&exclude_archived=1", SLACK_API_TOKEN);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        org.springframework.http.HttpEntity httpEntity = new org.springframework.http.HttpEntity(headers);
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        try {
            ResponseEntity<?> responseEntity = restTemplate.exchange(getChannelsEndPoint, HttpMethod.POST, httpEntity, String.class);
            for (JsonNode channelNode : getChannelsArray(responseEntity.getBody().toString())) {
                if(channelNode.get("name").toString().replaceAll("\"", "").equalsIgnoreCase(groupName))
                {
                    return channelNode.get("id").toString();
                }
            }
            return "GroupNotFound";
        } catch (HttpServerErrorException e) {
            LOGGER.log(Level.SEVERE, "Couldn't create group in slack", e);
            return "Error";
        }
    }

    private String closeChannel(String channelId){
        String closeChannel = String.format(Main.getEnvironmentOverlayProperties().get("bettercloud.slack.close.group") + "?token=%s&channel=%s", SLACK_API_TOKEN, channelId);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        org.springframework.http.HttpEntity httpEntity = new org.springframework.http.HttpEntity(headers);
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        try {
            ResponseEntity<?> responseEntity = restTemplate.exchange(closeChannel, HttpMethod.POST, httpEntity, String.class);
            return responseEntity.getBody().toString();
        } catch (HttpServerErrorException e) {
            LOGGER.log(Level.SEVERE, "Couldn't create group in slack", e);
            return "Error";
        }
    }

    private JsonNode getChannelsArray(String responseBody) {
        JsonNode memberNode = null;
        try {
            JsonNode root = new ObjectMapper().readTree(responseBody);
//            memberNode = root.path("channels");
            memberNode = root.path("groups");
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Problem occured on input output operations.");
            throw new RuntimeException(e);
        }
        return memberNode;
    }

}
