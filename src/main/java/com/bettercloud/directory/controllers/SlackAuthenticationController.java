package com.bettercloud.directory.controllers;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "Slack Authenticate", description = "Slack Authentication API")
@RestController
@RequestMapping(value = "/authenticate")
public class SlackAuthenticationController {

}
