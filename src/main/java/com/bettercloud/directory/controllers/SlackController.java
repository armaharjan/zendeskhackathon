package com.bettercloud.directory.controllers;

import com.bettercloud.directory.Main;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Level;
import java.util.logging.Logger;

@Api(value = "slack", description = "Slack SAAS API")
@RestController
@RequestMapping(value = "/slack")
public class SlackController {

    private static final Logger LOGGER = Logger.getLogger(SlackController.class.getSimpleName());
    private static final ObjectMapper MAPPER = new ObjectMapper();
    protected static final String SLACK_API_TOKEN = Main.getEnvironmentOverlayProperties().get("bettercloud.slack.apitoken");

    @Autowired
    GroupController groupController;

    @Autowired
    UserController userController;

    @Autowired
    RestTemplate restTemplate;

    @ApiOperation(value = "Create Group with user", notes = "Create Group as well as add user.")
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String inviteGroup(@RequestBody JsonNode ticketPayload){
        try{
            ObjectMapper mapper = new ObjectMapper();
            String ticketId = ticketPayload.path("ticketId").toString().replaceAll("\"", "");
            String groupCreateResult = groupController.createGroup(ticketId);
            JsonNode groupNode = mapper.readTree(groupCreateResult);
            String groupId = groupNode.path("group").get("id").toString().replaceAll("\"", "");
            String userId = userController.getUserId(ticketPayload.path("userId").toString()).replaceAll("\"", "");
            String inviteUserResponse = addUserToUserGroups(userId, groupId);
            String comment = String.format("%s%nby : %s", ticketPayload.path("comment").toString(), ticketPayload.path("customerEmail").toString());

            return writeMessageToChannel(groupId, comment);
        }catch (Exception e){
            return "Failed to open channel with user.";
        }
    }

    public String writeMessageToChannel(String groupId, String comment){

        comment = comment.replaceAll("\\n", "");
        comment = comment.replaceAll("-", "");
        String chatEndPoint = String.format(Main.getEnvironmentOverlayProperties().get("bettercloud.slack.chat.send.message") + "?token=%s&channel=%s&text=%s",
                SLACK_API_TOKEN,
                groupId.replace("\"", ""),
                comment);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");


        org.springframework.http.HttpEntity httpEntity = new org.springframework.http.HttpEntity(headers);
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        try {
            ResponseEntity<?> responseEntity = restTemplate.exchange(chatEndPoint, HttpMethod.POST, httpEntity, String.class);
            return responseEntity.getBody().toString();
        } catch (HttpServerErrorException e) {
            LOGGER.log(Level.SEVERE, "Couldn't write message in slack", e);
            return "Couldn't write message in slack";
        }
    }

    private String addUserToUserGroups(String userId, String groupId) {
        String addUserEndPoint = String.format(Main.getEnvironmentOverlayProperties().get("bettercloud.slack.invite.user.to.group") + "?token=%s&channel=%s&user=%s",
                SLACK_API_TOKEN,
                groupId.replace("\"", ""),
                userId.replace("\"", ""));
        HttpHeaders headers = new HttpHeaders();
//        headers.set("Accept", "application/json");
        headers.set("Content-Type", "text/plain");

        org.springframework.http.HttpEntity httpEntity = new org.springframework.http.HttpEntity(headers);
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        try {
            ResponseEntity<?> responseEntity = restTemplate.exchange(addUserEndPoint, HttpMethod.POST, httpEntity, String.class);
            return responseEntity.getBody().toString();
        } catch (HttpServerErrorException e) {
            LOGGER.log(Level.SEVERE, "Couldn't create group in slack", e);
            return "Couldn't create group in slack";
        }
    }


    @ApiOperation(value = "Send message by Customer", notes = "send message to slack from customer.")
    @RequestMapping(value = "/support/", method = RequestMethod.POST)
    public String sendMessage(@RequestBody JsonNode commentPayload){
        try{
            String ticketId = commentPayload.path("ticketId").toString().replaceAll("\"", "");
            String comment = commentPayload.path("comment").toString().replaceAll("\"", "");
            return writeMessageToChannel(ticketId, comment);
        }catch (Exception e){
            return "Failed to comment on channel.";
        }
    }
}

