package com.bettercloud.directory.controllers;

import com.bettercloud.directory.Main;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Slack outgoing webhook's here
 */
@Api(value = "App Command Service", description = "App Command Service API")
@RestController
@RequestMapping(value = "/appcommand")
public class AppCommandController {

    private static final String OUTGOING_WEBHOOK_TOKEN = Main.getEnvironmentOverlayProperties().get("bettercloud.slack.outgoing.webhook.token");

    @Autowired
    GroupController groupController;

    @Autowired
    ZendeskController zendeskController;

    @RequestMapping(value = "/zendesk", method = RequestMethod.POST)
    @ApiOperation(value = "Update Zendesk", notes = "Calls Zendesk end points")
    public String getAppCommands(@RequestParam(value = "text", required = true) String commands) {
        String appCommands = commands.replaceAll("\\s\\s+", " ").replaceAll("^\\s", "");
        String appKeyWord = commands.split("\\|")[0];
        String appParameters = commands.split("\\|")[1];
        String ticketNumber = commands.split("\\|")[2];
        switch (appKeyWord.toLowerCase()) {
            case ("update"):
                return zendeskController.update(appParameters + "|" + ticketNumber);
            case ("solved"):
                String zendeskResponse = zendeskController.solved(appParameters + "|" + ticketNumber);
                if(!zendeskResponse.contains("Error")){
                    return groupController.deleteGroup(ticketNumber);
                }
                return zendeskResponse;
            default:
                throw new IllegalArgumentException(String.format("%s slash command is not available.", appKeyWord));
        }
    }
}
