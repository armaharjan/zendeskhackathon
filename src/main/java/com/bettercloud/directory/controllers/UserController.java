package com.bettercloud.directory.controllers;

import com.bettercloud.directory.Main;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.http.HttpEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

@Api(value = "User", description = "Slack USER API")
@RestController
@RequestMapping(value = "/user")
public class UserController {

    private static final Logger LOGGER = Logger.getLogger(UserController.class.getSimpleName());
    private static final String PROXY_PATH = Main.getEnvironmentOverlayProperties().get("bettercloud.slack.userList");
    private static final String API_TOKEN = Main.getEnvironmentOverlayProperties().get("bettercloud.slack.apitoken");

    @Autowired
    RestTemplate restTemplate;

    @RequestMapping(value = "/getUsers", method = RequestMethod.GET)
    @ApiOperation(value = "Get Users", notes = "Get all the users that falls under provided index.")
    public JsonNode getUsers(@RequestParam(value = "index", required = true) String index) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        org.springframework.http.HttpEntity httpEntity = new org.springframework.http.HttpEntity(headers);
        String finalUrl = String.format(PROXY_PATH + "?token=%s", API_TOKEN);

        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        try {
            ResponseEntity<?> responseEntity = restTemplate.exchange(finalUrl, HttpMethod.GET, httpEntity, String.class);
            return getResourceJsonArray(responseEntity.getBody().toString());
        } catch (HttpServerErrorException e) {
            LOGGER.log(Level.SEVERE, "Couldn't get users from Slack", e);
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readTree("\"error\":\"Couldn't read users from slack\"");
        }
    }

    @RequestMapping(value = "/getUserId", method = RequestMethod.GET)
    @ApiOperation(value = "Get User", notes = "Get user that matches provided username.")
    public String getUserId(@RequestParam(value = "userName", required = true) String userName) {
        String userId = "";
        try {
            JsonNode usersNode = getUsers("300");
            for (JsonNode userNode : usersNode) {
                if (userNode.get("name").textValue().replaceAll("\"", "").equals(userName.replaceAll("\"", ""))) {
                    userId = userNode.get("id").textValue();
                }
            }

        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Problem occured on input output operations.");
            throw new RuntimeException(e);
        }
        return userId;
    }

    @RequestMapping(value = "/getUserIdByEmail", method = RequestMethod.GET)
    @ApiOperation(value = "Get User", notes = "Get user that matches provided username.")
    public String getUserIdByEmail(@RequestParam(value = "userEmail", required = true) String userEmail) {
        String userId = "";
        try {
            JsonNode usersNode = getUsers("300");
            for (JsonNode userNode : usersNode) {
                if (userNode.hasNonNull("profile") && userNode.path("profile").hasNonNull("email") && userNode.path("profile").get("email").textValue().replaceAll("\"", "").equals(userEmail.replaceAll("\"", ""))) {
                    userId = userNode.get("id").textValue();
                }
            }
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Problem occured on input output operations.");
            throw new RuntimeException(e);
        }
        return userId;
    }

    private JsonNode getResourceJsonArray(String responseBody) {
        JsonNode memberNode = null;
        try {
            JsonNode root = new ObjectMapper().readTree(responseBody);
            memberNode = root.path("members");
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Problem occured on input output operations.");
            throw new RuntimeException(e);
        }
        return memberNode;
    }

    public static String getJsonString(HttpEntity entity) {
        String result = "";
        try {
            LOGGER.log(Level.FINEST, "Reading request result content");
            InputStream content = null;
            content = entity.getContent();
            String line;
            BufferedReader reader = new BufferedReader(new InputStreamReader(content));
            StringBuilder builder = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                builder.append(line).append("\n");
            }
            LOGGER.log(Level.FINEST, "Finished reading request result content");
            result = builder.toString();
        } catch (JsonProcessingException e) {
            LOGGER.log(Level.SEVERE, "Problem occured while parsing user json.");
            throw new RuntimeException(e);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Problem occured on input output operations.");
            throw new RuntimeException(e);
        }
        return result;
    }
}
