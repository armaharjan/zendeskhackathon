package com.bettercloud.directory;

import com.bettercloud.environment.exception.LoadEnvironmentOverlayException;
import com.bettercloud.environment.util.EnvironmentPropertiesLoader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.concurrent.ConcurrentMap;
import java.util.logging.Level;
import java.util.logging.Logger;

@SpringBootApplication
@EnableSwagger2
public class Main {
    protected static Logger log = Logger.getLogger(Main.class.getSimpleName());

    private static ConcurrentMap<String, String> environmentOverlayProperties;

    private static EnvironmentPropertiesLoader propertiesLoader = new EnvironmentPropertiesLoader();

    public static final String SERVICE_NAME = "zendeskHackathon";

    public static void main(String... args) {
        ConcurrentMap<String, String> environmentOverlayProperties = getEnvironmentOverlayProperties();
        if (environmentOverlayProperties == null || environmentOverlayProperties.isEmpty()) {
            log.log(Level.SEVERE, "No environment properties loaded. DEATH has befallen us!!!");
            return;
        }

        SpringApplication.run(Main.class, args);
    }

    public static ConcurrentMap<String, String> getEnvironmentOverlayProperties() {
        try {
            if (environmentOverlayProperties == null) {
                setEnvironmentOverlayProperties(propertiesLoader.loadEvironmentOverlays(SERVICE_NAME));
            }
        } catch (LoadEnvironmentOverlayException e) {
            log.log(Level.SEVERE, "Failed to load environment properties", e);
        }

        return environmentOverlayProperties;
    }

    /**
     * Only used for unit testing, get will populate the properties for production code.
     *
     * @param environmentOverlayProperties Properties
     */
    public static void setEnvironmentOverlayProperties(ConcurrentMap<String, String> environmentOverlayProperties) {
        Main.environmentOverlayProperties = environmentOverlayProperties;
    }
}
