package com.bettercloud.directory.configs;

import com.bettercloud.directory.controllers.GroupController;
import com.bettercloud.directory.controllers.SlackController;
import com.bettercloud.directory.controllers.UserController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ControllerConfig {
    @Bean
    public GroupController groupController() {
        return new GroupController();
    }

    @Bean
    public UserController userController() {
        return new UserController();
    }

    @Bean
    public SlackController slackController(){
        return new SlackController();
    }
}
